
import java.util.Map;
import java.util.TreeMap;

public class RomanNumber {
    private TreeMap<Integer, String> values = new TreeMap<>();

    private String roman;

    public RomanNumber(int n) {
        roman = "";

        values.put(1, "I");
        values.put(4, "IV");
        values.put(5, "V");
        values.put(9, "IX");
        values.put(10, "X");
        values.put(40, "XL");
        values.put(50, "L");
        values.put(90, "XC");
        values.put(100, "C");
        values.put(400, "CD");
        values.put(500, "D");
        values.put(900, "CM");
        values.put(1000, "M");

        if (n > 0 && n <= 9999) {
            Map.Entry<Integer, String> entry;
            while (n > 0){
                entry = values.floorEntry(n);
                roman += entry.getValue();
                n -= values.floorKey(n);
            }
        }
        else
            roman = "ERROR : Number must be between 1 and 9999";
    }

    public String getRomanLetter(Integer n){
        return values.get(n);
    }

    @Override
    public String toString() {
        return roman;
    }
}
