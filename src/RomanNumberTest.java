import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RomanNumberTest {
    @Test
    public void testConvertsTheNumbersToString() {
        RomanNumber roman = new RomanNumber(1);
        assertEquals("I", roman.toString());

        RomanNumber roman1 = new RomanNumber(2);
        assertEquals("II", roman1.toString());
    }

    @Test
    public void testRomanLetterEqualsRightValue(){
        RomanNumber roman = new RomanNumber(1);
        assertEquals("L", roman.getRomanLetter(50));
        assertEquals("I", roman.getRomanLetter(1));
        assertEquals("C", roman.getRomanLetter(100));
        assertEquals("M", roman.getRomanLetter(1000));
        assertEquals("V", roman.getRomanLetter(5));
    }

    @Test
    public void testNumberBetween1And9999(){
        RomanNumber roman = new RomanNumber(10000);
        assertEquals("ERROR : Number must be between 1 and 9999", roman.toString());

        RomanNumber roman1 = new RomanNumber(-1);
        assertEquals("ERROR : Number must be between 1 and 9999", roman1.toString());

        RomanNumber roman2 = new RomanNumber(0);
        assertEquals("ERROR : Number must be between 1 and 9999", roman2.toString());
    }

    @Test
    public void testLettersAddIfValueIsSuperior(){
        RomanNumber roman = new RomanNumber(7);
        assertEquals("VII", roman.toString());

        RomanNumber roman1 = new RomanNumber(16);
        assertEquals("XVI", roman1.toString());

        RomanNumber roman2 = new RomanNumber(112);
        assertEquals("CXII", roman2.toString());
    }

    @Test
    public void testLetterBeforeAnotherToSubstract(){
        RomanNumber roman = new RomanNumber(9);
        assertEquals("IX", roman.toString());

        RomanNumber roman1 = new RomanNumber(44);
        assertEquals("XLIV", roman1.toString());

        RomanNumber roman2 = new RomanNumber(99);
        assertEquals("XCIX", roman2.toString());
    }

    @Test
    public void testSymbolGroupedByDescendingOrder(){
        RomanNumber roman = new RomanNumber(2958);
        assertEquals("MMCMLVIII", roman.toString());
    }
}
